package max.exercises;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


public class ExercisesTests {


	@Test
	void test1000() {
		
		assertEquals("Hello World!", Exercises.exercicio1000());
	}
	
	@Test
	void test1001() {
		
		int A = 5;
		int B = 5;
		
		int sum = A + B;
		
		String expected = "X = " + sum;
		
		assertEquals(expected, Exercises.exercicio1001());
	}
}
