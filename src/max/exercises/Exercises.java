package max.exercises;
import java.util.Scanner;

public class Exercises {
		
	static Scanner sc = new Scanner(System.in);

	
	public static String exercicio1000() {
		
		System.out.println("Hello World!");
		
        return "Hello World!";
	}
	
	public static String exercicio1001() {
		
		
		int A = sc.nextInt();
		int B = sc.nextInt();
		
		int X = A + B;
		
		sc.close();
		
		return ("X = " + X);
	}
	
	public static void exercicio1002() {
				
		double raio = sc.nextDouble();
		
		sc.close();
		
		double area =  3.14159 * (Math.pow(raio, 2));
		
		System.out.printf("A=%.4f%n", area);
		
	}
	
	public static void exercicio1003() {
		
		int A = sc.nextInt();
		int B = sc.nextInt();
		
		sc.close();
		
		int SOMA = A + B;
		
		System.out.printf("SOMA = %d%n", SOMA);
		
	}
	

	public static void exercicio1004() {
		
		int A = sc.nextInt();
		int B = sc.nextInt();
		
		sc.close();
		
		int PROD = A * B;
		
		System.out.printf("PROD = %d%n", PROD);
		
	}
	
	
	public static void exercicio1005() {
		
		double A = sc.nextDouble();
		double B = sc.nextDouble();
		
		sc.close();
		
		double MEDIA = ((A * 3.5) + (B * 7.5))/11.0;
		
		System.out.printf("MEDIA = %.5f%n", MEDIA);
		
	}
	
	public static void exercicio1006() {
		
		double A = sc.nextDouble();
		double B = sc.nextDouble();
		double C = sc.nextDouble();
		
		
		sc.close();
		
		double MEDIA = ((A * 2) + (B * 3) + (C * 5))/10.0;
		
		System.out.printf("MEDIA = %.1f%n", MEDIA);
	}
	
	public static void exercicio1007() {
		
		int A = sc.nextInt();
		int B = sc.nextInt();
		int C = sc.nextInt();
		int D = sc.nextInt();
		
		sc.close();
		
		int DIFERENCA = (A * B - C * D);
		
		System.out.printf("DIFERENCA = %d%n", DIFERENCA);
		
	}
	
	public static void exercicio1008() {
		
		int number = sc.nextInt();
		int hours = sc.nextInt();
		double perHour = sc.nextDouble();
		
		sc.close();
		
		double salary = hours * perHour;
		
		System.out.printf("NUMBER = %d%nSALARY = U$ %.2f%n", number, salary);
		
	}
	
	public static void exercicio1009() {
		
		String name = sc.nextLine();
		double fixedSalary = sc.nextDouble();
		double montant = sc.nextDouble();
		double bonus = 0.15;
		
		sc.close();
		
		double finalSalary = fixedSalary + (montant * bonus);
		
		System.out.printf("TOTAL = R$ %.2f%n", finalSalary);
		
	}
	
	public static void exercicio1010() {
		
		int cod1 = sc.nextInt();
		int quantity1 = sc.nextInt();
		double price1 = sc.nextDouble();
		
		sc.nextLine();
		
		int cod2 = sc.nextInt();
		int quantity2 = sc.nextInt();
		double price2 = sc.nextDouble();
		
		
		double total = (quantity1 * price1) + (quantity2 * price2);
		
		
		System.out.printf("VALOR A PAGAR: R$ %.2f%n", total);
		
	}
}
